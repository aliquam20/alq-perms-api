package org.aliquam.perms.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Note: "global" is the default context
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqPermContext {
    private String server;
    private String world;
}
