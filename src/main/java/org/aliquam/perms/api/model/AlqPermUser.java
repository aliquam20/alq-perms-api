package org.aliquam.perms.api.model;

import lombok.Data;

import java.util.UUID;

@Data
public class AlqPermUser {
    private UUID id;
}
