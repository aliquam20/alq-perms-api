package org.aliquam.perms.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqPermTrack {
    private String name;
    private List<String> groups;

    public static AlqPermTrack create(String name, AlqPermGroup... groups) {
        return new AlqPermTrack(name, Arrays.stream(groups).map(AlqPermGroup::getName).collect(Collectors.toList()));
    }

    public static AlqPermTrack create(String name, List<AlqPermGroup> groups) {
        return new AlqPermTrack(name, groups.stream().map(AlqPermGroup::getName).collect(Collectors.toList()));
    }
}
