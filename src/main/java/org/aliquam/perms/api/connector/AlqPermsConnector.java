package org.aliquam.perms.api.connector;

import com.fasterxml.jackson.core.type.TypeReference;
import org.aliquam.cum.network.AlqConnector;
import org.aliquam.cum.network.AlqRequestBuilder;
import org.aliquam.cum.network.AlqUrl;
import org.aliquam.perms.api.model.AlqPerm;
import org.aliquam.perms.api.model.AlqPermContext;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.perms.api.model.AlqPermTrack;
import org.aliquam.perms.api.model.AlqPermTrackMoteUserRequest;
import org.aliquam.perms.api.model.AlqPermUser;
import org.aliquam.perms.api.model.ModifyInheritedGroupRequest;
import org.aliquam.session.api.connector.AlqSessionManager;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@SuppressWarnings({"unused", "Convert2Diamond"})
public class AlqPermsConnector {
    private static final AlqUrl alqPermsService = AlqConnector.of("perms");
    private static final AlqUrl groupResource = alqPermsService.resolve("group");
    private static final AlqUrl userResource = alqPermsService.resolve("user");
    private static final AlqUrl trackResource = alqPermsService.resolve("track");

    private final AlqSessionManager sessionManager;

    public AlqPermsConnector(AlqSessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public List<AlqPermGroup> listGroups() {
        return new AlqRequestBuilder(groupResource)
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<AlqPermGroup>>() {});
    }

    public AlqPermGroup getGroup(String name) {
        return new AlqRequestBuilder(groupResource.resolve(name))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(AlqPermGroup.class);
    }

    public AlqPermGroup putGroup(AlqPermGroup group) {
        return new AlqRequestBuilder(groupResource.resolve(group.getName()))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .PUT(group)
                .build().execute(AlqPermGroup.class);
    }

    public void deleteGroup(AlqPermGroup group) {
        deleteGroup(group.getName());
    }

    public void deleteGroup(String name) {
        new AlqRequestBuilder(groupResource.resolve(name))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .DELETE()
                .build().execute();
    }

    public List<AlqPermGroup> getGroupInheritedGroups(AlqPermGroup group, AlqPermContext context) {
        return getGroupInheritedGroups(group.getName(), context);
    }

    public List<AlqPermGroup> getGroupInheritedGroups(String groupName, AlqPermContext context) {
        return new AlqRequestBuilder(withContext(groupResource.resolve(groupName, "inherited"), context))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<AlqPermGroup>>() {});
    }

    public void addGroupInheritedGroup(AlqPermGroup group, AlqPermGroup inheritedGroup, AlqPermContext context) {
        addGroupInheritedGroup(group.getName(), new ModifyInheritedGroupRequest(inheritedGroup.getName(), context));
    }

    public void addGroupInheritedGroup(AlqPermGroup group, ModifyInheritedGroupRequest inheritedGroupRequest) {
        addGroupInheritedGroup(group.getName(), inheritedGroupRequest);
    }

    public void addGroupInheritedGroup(String groupName, ModifyInheritedGroupRequest inheritedGroupRequest) {
        new AlqRequestBuilder(groupResource.resolve(groupName, "inherited", "add"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(inheritedGroupRequest)
                .build().execute();
    }

    public void removeGroupInheritedGroup(AlqPermGroup group, AlqPermGroup inheritedGroup, AlqPermContext context) {
        removeGroupInheritedGroup(group.getName(), new ModifyInheritedGroupRequest(inheritedGroup.getName(), context));
    }

    public void removeGroupInheritedGroup(AlqPermGroup group, ModifyInheritedGroupRequest inheritedGroupRequest) {
        removeGroupInheritedGroup(group.getName(), inheritedGroupRequest);
    }

    public void removeGroupInheritedGroup(String groupName, ModifyInheritedGroupRequest inheritedGroupRequest) {
        new AlqRequestBuilder(groupResource.resolve(groupName, "inherited", "remove"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(inheritedGroupRequest)
                .build().execute();
    }

    public List<AlqPerm> listGroupPermissions(AlqPermGroup group) {
        return listGroupPermissions(group.getName());
    }

    public List<AlqPerm> listGroupPermissions(String groupName) {
        return new AlqRequestBuilder(groupResource.resolve(groupName, "permissions"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<AlqPerm>>() {});
    }

    public void addGroupPermission(AlqPermGroup group, String permission, AlqPermContext context) {
        addGroupPermission(group.getName(), new AlqPerm(permission, context));
    }

    public void addGroupPermission(AlqPermGroup group, AlqPerm permission) {
        addGroupPermission(group.getName(), permission);
    }

    public void addGroupPermission(String groupName, AlqPerm permission) {
        new AlqRequestBuilder(groupResource.resolve(groupName, "permissions", "add"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(permission)
                .build().execute();
    }

    public void removeGroupPermission(AlqPermGroup group, String permission, AlqPermContext context) {
        removeGroupPermission(group.getName(), new AlqPerm(permission, context));
    }

    public void removeGroupPermission(AlqPermGroup group, AlqPerm permission) {
        removeGroupPermission(group.getName(), permission);
    }

    public void removeGroupPermission(String groupName, AlqPerm permission) {
        new AlqRequestBuilder(groupResource.resolve(groupName, "permissions", "remove"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(permission)
                .build().execute();
    }

    public List<AlqPermUser> listUsers() {
        return new AlqRequestBuilder(userResource)
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<AlqPermUser>>() {});
    }

    public AlqPermUser getUser(UUID id) {
        return new AlqRequestBuilder(userResource.resolve(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(AlqPermUser.class);
    }

    public void deleteUser(UUID id) {
        new AlqRequestBuilder(userResource.resolve(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .DELETE()
                .build().execute();
    }

    public List<AlqPermGroup> getUserInheritedGroups(UUID id, AlqPermContext context) {
        return new AlqRequestBuilder(withContext(userResource.resolve(id, "inherited"), context))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<AlqPermGroup>>() {});
    }

    public void addUserInheritedGroup(UUID id, AlqPermGroup inheritedGroup, AlqPermContext context) {
        addUserInheritedGroup(id, new ModifyInheritedGroupRequest(inheritedGroup.getName(), context));
    }

    public void addUserInheritedGroup(UUID id, ModifyInheritedGroupRequest inheritedGroupRequest) {
        new AlqRequestBuilder(userResource.resolve(id, "inherited", "add"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(inheritedGroupRequest)
                .build().execute();
    }

    public void removeUserInheritedGroup(UUID id, AlqPermGroup inheritedGroup, AlqPermContext context) {
        removeUserInheritedGroup(id, new ModifyInheritedGroupRequest(inheritedGroup.getName(), context));
    }

    public void removeUserInheritedGroup(UUID id, ModifyInheritedGroupRequest inheritedGroupRequest) {
        new AlqRequestBuilder(userResource.resolve(id, "inherited", "remove"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(inheritedGroupRequest)
                .build().execute();
    }

    public List<AlqPerm> listUserPermissions(UUID id) {
        return new AlqRequestBuilder(userResource.resolve(id, "permissions"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<AlqPerm>>() {});
    }

    public void addUserPermission(UUID id, String permission, AlqPermContext context) {
        addUserPermission(id, new AlqPerm(permission, context));
    }

    public void addUserPermission(UUID id, AlqPerm permission) {
        new AlqRequestBuilder(userResource.resolve(id, "permissions", "add"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(permission)
                .build().execute();
    }

    public void removeUserPermission(UUID id, String permission, AlqPermContext context) {
        removeUserPermission(id, new AlqPerm(permission, context));
    }

    public void removeUserPermission(UUID id, AlqPerm permission) {
        new AlqRequestBuilder(userResource.resolve(id, "permissions", "remove"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(permission)
                .build().execute();
    }

    public boolean hasUserPermission(UUID id, String permission, AlqPermContext context) {
        return new AlqRequestBuilder(withContext(userResource.resolve(id, "permissions", permission, "has"), context))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(Boolean.class);
    }

    public List<AlqPermTrack> listTracks() {
        return new AlqRequestBuilder(trackResource)
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<AlqPermTrack>>() {});
    }

    public AlqPermTrack getTrack(String name) {
        return new AlqRequestBuilder(trackResource.resolve(name))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(AlqPermTrack.class);
    }

    public AlqPermTrack putTrack(AlqPermTrack alqTrack) {
        return new AlqRequestBuilder(trackResource.resolve(alqTrack.getName()))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .PUT(alqTrack)
                .build().execute(AlqPermTrack.class);
    }

    public void deleteTrack(AlqPermTrack track) {
        deleteTrack(track.getName());
    }

    public void deleteTrack(String name) {
        new AlqRequestBuilder(trackResource.resolve(name))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .DELETE()
                .build().execute();
    }

    public void promote(UUID userId, AlqPermGroup currentGroup, AlqPermTrack track) {
        promote(userId, currentGroup.getName(), track.getName());
    }

    public void promote(UUID userId, String currentGroupName, String trackName) {
        promote(new AlqPermTrackMoteUserRequest(userId, currentGroupName), trackName);
    }

    public void promote(AlqPermTrackMoteUserRequest request, String trackName) {
        new AlqRequestBuilder(trackResource.resolve(trackName, "promote"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(request)
                .build().execute();
    }

    public void demote(UUID userId, AlqPermGroup currentGroup, AlqPermTrack track) {
        demote(userId, currentGroup.getName(), track.getName());
    }

    public void demote(UUID userId, String currentGroupName, String trackName) {
        demote(new AlqPermTrackMoteUserRequest(userId, currentGroupName), trackName);
    }

    public void demote(AlqPermTrackMoteUserRequest request, String trackName) {
        new AlqRequestBuilder(trackResource.resolve(trackName, "demote"))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(request)
                .build().execute();
    }

    private static AlqUrl withContext(AlqUrl url, AlqPermContext context) {
        AlqUrl ret = url;
        if(context.getServer() != null) {
            ret = ret.queryParam("server", context.getServer());
        }
        if(context.getWorld() != null) {
            ret = ret.queryParam("world", context.getWorld());
        }
        return ret;
    }

}
