package org.aliquam.perms.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModifyInheritedGroupRequest {
    private String inheritedGroupName;
    private AlqPermContext context;

    public ModifyInheritedGroupRequest(AlqPermGroup inheritedGroupName, AlqPermContext context) {
        this(inheritedGroupName.getName(), context);
    }
}
