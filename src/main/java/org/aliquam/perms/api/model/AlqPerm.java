package org.aliquam.perms.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqPerm {
    String permission;
    AlqPermContext context;
}
