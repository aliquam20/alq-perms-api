package org.aliquam.perms.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqPermTrackMoteUserRequest {
    private UUID user;
    private String currentGroup;
}
